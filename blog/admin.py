# -*- encoding: utf-8 -*-

from django.contrib import admin
from blog.models import Post

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('author', 'title', 'created_date', 'published_date')
    list_filter = ('author', 'title', 'created_date', 'published_date')
    search_fields = ['author']
admin.site.register(Post, PostAdmin)

