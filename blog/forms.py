# -*- encoding: utf-8 -*-

from django import forms
from blog.models import Post

# Create your forms here.

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text',)